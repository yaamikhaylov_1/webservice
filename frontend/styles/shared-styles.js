// eagerly import theme styles so as we can override them
import '@vaadin/vaadin-lumo-styles/all-imports';

const $_documentContainer = document.createElement('template');

$_documentContainer.innerHTML = `
<custom-style>
<style>
    
    [theme~="dark"] {
      --lumo-base-color: hsl(138, 67%, 12%);
      --lumo-primary-color: hsl(3, 85%, 50%);
      --lumo-card-color: hsl(5,65%,66%)
    }

</style>
</custom-style>


`;

document.head.appendChild($_documentContainer.content);
