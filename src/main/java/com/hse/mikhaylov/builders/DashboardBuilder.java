package com.hse.mikhaylov.builders;

import com.hse.mikhaylov.model.Industry;
import com.hse.mikhaylov.model.LookupTable;
import com.hse.mikhaylov.views.dashboard.WrapperCard;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.board.Board;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.ColorAxis;
import com.vaadin.flow.component.charts.model.PlotOptionsTreemap;
import com.vaadin.flow.component.charts.model.TreeMapLayoutAlgorithm;
import com.vaadin.flow.component.charts.model.TreeSeriesItem;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class DashboardBuilder {

    private List<Industry> data;

    private int minElementsPerRow;


    public static DashboardBuilder get(){
        return new DashboardBuilder();
    }

    public DashboardBuilder withData(List data) {
        this.data = data;
        return this;
    }

    public DashboardBuilder withMinElementsPerRow(int minElementsPerRow) {
        this.minElementsPerRow = minElementsPerRow;
        return this;
    }

    public Board build(){
        Board board = new Board();

        board.addRow(generateRow(1));

        return board;
    }

    private Component[] generateRow(int num){
        Component[] result = new Component[num];
        for(int i = 0; i < num;i++){
            result[i] = generateRowElement();
        }

        return result;
    }

    private Component generateRowElement(){
        data.sort(Comparator.comparing(Industry::getCap).reversed());
        PlotOptionsTreemap options = new PlotOptionsTreemap();
        options.setAllowPointSelect(true);

        Chart chart = TreeMapBuilder.get()
                .withColorAxis(new ColorAxis())
                .withPlotOptions(options)
                .withMinColor("#c23236")
                .withMaxColor("#31894e")
                .withTitle("S&P500")
                .withStops(0f,25f,50f,75f,100f)
                .withLayoutAlgorithm(TreeMapLayoutAlgorithm.SQUARIFIED)
                .withData(data)
                .build();


        chart.addPointSelectListener(listener->{
            TreeSeriesItem selectedItem;
            selectedItem = (TreeSeriesItem) LookupTable.getSeriesById(listener.getSeries().getId()).getData().toArray()[listener.getItemIndex()];

            System.out.println("Selected: "+ LookupTable.getCompanyByTicker(selectedItem.getName()));
            LookupTable.getSeriesById(listener.getSeries().getId()).updateSeries();

        });

        chart.addPointUnselectListener(listener->{
            TreeSeriesItem selectedItem;
            selectedItem = (TreeSeriesItem) LookupTable.getSeriesById(listener.getSeries().getId()).getData().toArray()[listener.getItemIndex()];

            System.out.println("Unselected: "+ LookupTable.getCompanyByTicker(selectedItem.getName()));
            LookupTable.getSeriesById(listener.getSeries().getId()).updateSeries();

        });





        return new WrapperCard("wrapper",new Component[]{chart}, "card");
    }

    /*

    public Board build(){
        Board board = new Board();
        data.sort(Comparator.comparing(Industry::getCap).reversed());
        int elementsPerRow;

        for(int i = 0; i < data.size(); i += elementsPerRow){
            int MAXELEMENTSPERROW = 4;
            elementsPerRow = 4;//new Random().nextInt(MAXELEMENTSPERROW - minElementsPerRow) + minElementsPerRow;
            board.addRow(generateRow(i,elementsPerRow));
        }

        return board;
    }

    private Component[] generateRow(int position,int elements){
        Component[] result = new Component[elements];
        for(int i = position; i < position + elements; i++){
            result[i - position] = generateRowElement(data.get(i));
        }
        return result;
    }

    private Component generateRowElement(Industry industry){
        PlotOptionsTreemap options = new PlotOptionsTreemap();
        options.setAllowPointSelect(true);
        Chart chart = TreeMapBuilderService.get()
                .withColorAxis(new ColorAxis())
                .withPlotOptions(options)
                .withMinColor("#c23236")
                .withMaxColor("#31894e")
                .withTitle(industry.getName())
                .withLayoutAlgorithm(TreeMapLayoutAlgorithm.SQUARIFIED)
                .withData(industry.getCompaniesAsList())
                .build();

        chart.addPointSelectListener(listener->{
            TreeSeriesItem temp;
            temp = (TreeSeriesItem) LookupTable.getSeriesById(listener.getSeries().getId()).getData().toArray()[listener.getItemIndex()];
            System.out.println(LookupTable.getCompanyByTicker(temp.getName()));

        });


        return new WrapperCard("wrapper",new Component[]{chart}, "card");
    }

     */

}
