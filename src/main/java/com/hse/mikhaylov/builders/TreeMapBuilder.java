package com.hse.mikhaylov.builders;

import com.hse.mikhaylov.model.Company;
import com.hse.mikhaylov.model.Industry;
import com.hse.mikhaylov.model.LookupTable;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.*;
import com.vaadin.flow.component.charts.model.style.SolidColor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreeMapBuilder {

    private List<Industry> data;

    private ColorAxis colorAxis;
    private PlotOptionsTreemap plotOptions;
    private String chartTitle;
    private Stop[] stops;

    public static TreeMapBuilder get(){
        return new TreeMapBuilder();
    }

    public TreeMapBuilder withData(List data) {
        this.data = data;
        return this;
    }

    public TreeMapBuilder withColorAxis(ColorAxis colorAxis) {
        this.colorAxis = colorAxis;
        return this;
    }

    public TreeMapBuilder withPlotOptions(PlotOptionsTreemap plotOptions) {
        this.plotOptions = plotOptions;
        return this;
    }

    public TreeMapBuilder withMinColor(String colorCode) {
        this.colorAxis.setMinColor(new SolidColor(colorCode));
        return this;
    }

    public TreeMapBuilder withMaxColor(String colorCode) {
        this.colorAxis.setMaxColor(new SolidColor(colorCode));
        return this;
    }

    public TreeMapBuilder withColorAxisVisibility(boolean bool) {
        this.colorAxis.setVisible(bool);
        return this;
    }

    public TreeMapBuilder withStops(Float... stops) {
        this.stops = new Stop[stops.length];
        for(int i = 0; i < stops.length;i++){
            this.stops[i] = new Stop(stops[i]);
        }
        return this;
    }

    public TreeMapBuilder withLayoutAlgorithm(TreeMapLayoutAlgorithm algorithm) {
        this.plotOptions.setLayoutAlgorithm(algorithm);
        return this;
    }

    public TreeMapBuilder withTitle(String title) {
        this.chartTitle = title;
        return this;
    }



    public Chart build(){
        Chart chart = new Chart(ChartType.TREEMAP);

        Level level = new Level();
        level.setLevel(1);
        level.setLayoutAlgorithm(TreeMapLayoutAlgorithm.SLICEANDDICE);

        DataLabels dataLabels = new DataLabels();
        dataLabels.setEnabled(true);
        dataLabels.setAlign(HorizontalAlign.LEFT);
        dataLabels.setVerticalAlign(VerticalAlign.TOP);

        TreeSeries series = generateSeries();
        level.setDataLabels(dataLabels);
        plotOptions.setLevels(level);
        colorAxis.setVisible(false);
        chart.getConfiguration().getTooltip().setEnabled(true);
        chart.getConfiguration().getTooltip().setValueSuffix("$");
        chart.getConfiguration().addColorAxis(colorAxis);
        chart.getConfiguration().addPlotOptions(plotOptions);
        chart.getConfiguration().addSeries(series);
        chart.getConfiguration().setTitle("");

        return chart;
    }

    private TreeSeries generateSeries(){
        TreeSeries series = new TreeSeries();

        for (Industry industry:data) {
            TreeSeriesItem industryItem = new TreeSeriesItem(industry.getName(),industry.getName());
            series.add(industryItem);
            for(Company comp:industry.getCompaniesAsList()){
                TreeSeriesItem compItem = new TreeSeriesItem(comp.getTicker(),industryItem, comp.getCap());
                compItem.setColorValue(comp.getIndex());
                series.add(compItem);
            }
            series.setId(chartTitle);
            LookupTable.addSeries(series.getId(),series);
        }


        /*
        List<TreeSeriesItem> items = new ArrayList<>();
        TreeSeriesItem tempItem;
        Company comp;

        for(Object o:data){
            comp = (Company)o;
            tempItem = new TreeSeriesItem(comp.getTicker(),comp.getCap());
            tempItem.setColorValue(comp.getIndex());
            items.add(tempItem);
        }

        TreeSeries series = new TreeSeries();
        series.setId(chartTitle);
        series.setData(items);

        LookupTable.addSeries(series.getId(),series);

         */
        return series;
    }
}
