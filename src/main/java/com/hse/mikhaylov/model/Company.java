package com.hse.mikhaylov.model;

import java.util.Objects;

public class Company {

    private final String name;
    private final String ticker;

    private int index;
    private int cap;

    public Company() {
        name = null;
        ticker = null;
        index = 0;
        cap = 0;
    }

    public Company(String name, String ticker, int index,int cap) {
        this.name = name;
        this.ticker = ticker;
        this.index = index;
        this.cap = cap;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getName() {
        return name;
    }

    public String getTicker() {
        return ticker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        Company company = (Company) o;
        return name.equals(company.name) &&
                ticker.equals(company.ticker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ticker);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Company{");
        sb.append("name='").append(name).append('\'');
        sb.append(", ticker='").append(ticker).append('\'');
        sb.append(", index=").append(index);
        sb.append(", market cap=").append(cap);
        sb.append('}');
        return sb.toString();
    }
}
