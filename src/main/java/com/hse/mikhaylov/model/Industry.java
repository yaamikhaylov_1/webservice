package com.hse.mikhaylov.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Industry {
    private final String name;
    private int cap;

    private HashMap<String,Company> companies;

    public Industry() {
        name = null;
        cap = 0;
    }

    public Industry(String name) {
        this.name = name;
        cap = 0;
        companies = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public int getCap() {
        return cap;
    }

    public void addCompany(Company company){
        companies.put(company.getTicker(),company);
        cap += company.getCap();
    }

    public Company getCompany(String ticker){
        return companies.get(ticker);
    }

    public HashMap<String, Company> getCompanies() {
        return companies;
    }

    public List<Company> getCompaniesAsList(){
        return new ArrayList<>(companies.values());
    }

}
