package com.hse.mikhaylov.model;

public class LookupRecord {
    private Company company;
    private final String industry;

    public LookupRecord(Company company, String industry) {
        this.company = company;
        this.industry = industry;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getIndustry() {
        return industry;
    }

    public String getKey(){
        return company.getTicker();
    }
}
