package com.hse.mikhaylov.model;

import com.vaadin.flow.component.charts.model.TreeSeries;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
public class LookupTable {

    private static HashMap<String,LookupRecord> companyLookup;
    private static HashMap<String, TreeSeries> seriesLookup = new HashMap<>();

    public static void setCompanyLookup(HashMap<String, LookupRecord> companyLookup) {
        LookupTable.companyLookup = companyLookup;
    }

    public void test(){
        System.out.println("Lookup tested!");
    }

    public void setTableFromList(List<Industry> industries){
        companyLookup = new HashMap<>();
        for (Industry industry : industries){
            for(Company company : industry.getCompaniesAsList()){
                LookupRecord record = new LookupRecord(company,industry.getName());
                companyLookup.put(record.getKey(),record);
            }
        }
    }

    public static Company getCompanyByTicker(String ticker){
        return companyLookup.get(ticker).getCompany();
    }

    public static void addSeries(String id, TreeSeries series){
        seriesLookup.put(id,series);
    }

    public static TreeSeries getSeriesById(String id){
        return seriesLookup.get(id);
    }

}
