package com.hse.mikhaylov.services;

import com.hse.mikhaylov.model.Company;
import com.hse.mikhaylov.model.Industry;
import com.hse.mikhaylov.util.CompanyFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

@Service
public class FileLoaderService {

    public void test(){
        System.out.println("Right here and working yay!");
    }

    public List<Industry> loadData(String filepath) {

        List<Industry> data = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(new File(filepath)),
                        Charset.forName("UTF-8")));
             Scanner fin = new Scanner(reader))
        {
            Industry industry = null;
            while (fin.hasNextLine()) {
                String line = fin.nextLine();
                if (line.startsWith("#") || line.isEmpty()) {
                    continue;
                }
                else if (line.equals("INDUSTRY")){
                    line = fin.nextLine();
                    data.add(new Industry(line));
                    industry = data.get(data.size()-1);
                } else {
                    String[] temp = line.split(" ");
                    Company company = CompanyFactory.createCompany(temp[0], temp[1]);
                    company.setIndex(Integer.parseInt(temp[2]));
                    company.setCap(Integer.parseInt(temp[3]));
                    assert industry != null;
                    industry.addCompany(company);
                }
            }
        } catch (IOException e) {
            System.out.println("Cannot find file at: " + filepath);
        }

        return data;
    }
}
