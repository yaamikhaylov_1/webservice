package com.hse.mikhaylov.util;

import com.hse.mikhaylov.model.Company;

public class CompanyFactory {
    public static Company createCompany(String name, String ticker){
        return new Company(name, ticker, 0,0);
    }
}
