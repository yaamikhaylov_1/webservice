package com.hse.mikhaylov.views.dashboard;

import com.hse.mikhaylov.builders.DashboardBuilder;
import com.hse.mikhaylov.services.FileLoaderService;
import com.hse.mikhaylov.views.main.MainView;
import com.vaadin.flow.component.board.Board;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;

//#31894e - max
//#c23236 - min

@Route(value = "index", layout = MainView.class)
@PageTitle("Dashboard")
@CssImport(value = "./styles/views/dashboard/dashboard-view.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
@RouteAlias(value = "", layout = MainView.class)
public class DashboardView extends Div implements AfterNavigationObserver {

    @Autowired
    private FileLoaderService fileLoader;

    public DashboardView() {
        setId("dashboard-view");
        Board board = DashboardBuilder.get()
                .withData(fileLoader.loadData("E:\\HSE\\Project\\financialindex\\src\\main\\resources\\text\\data.txt"))
                .withMinElementsPerRow(2)
                .build();

        add(board);
    }


    @Override
    public void afterNavigation(AfterNavigationEvent event) {

    }

}
